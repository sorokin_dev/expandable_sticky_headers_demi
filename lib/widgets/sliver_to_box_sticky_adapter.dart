import 'dart:math' as math;

import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

/// This sliver renders [child] Widget sticky in [CustomScrollView].
/// When widget reaches the start edge of viewport, this sliver stays pinned.
/// Behavior is similar to [SliverAppBar], but this sliver doesn't take
/// the extent of the [child] explicitly.
class SliverToBoxStickyAdapter extends SingleChildRenderObjectWidget {
  const SliverToBoxStickyAdapter({
    Key key,
    Widget child,
  }) : super(key: key, child: child);

  @override
  RenderSliverToBoxStickyAdapter createRenderObject(BuildContext context) => RenderSliverToBoxStickyAdapter();
}


class RenderSliverToBoxStickyAdapter extends RenderSliverSingleBoxAdapter {

  RenderSliverToBoxStickyAdapter({
    RenderBox child,
  }) : super(child: child);

  @override
  void performLayout() {
    if (child == null) {
      geometry = SliverGeometry.zero;
      return;
    }

    final constraints = this.constraints;
    child.layout(constraints.asBoxConstraints(), parentUsesSize: true);

    final childExtent = _getChildExtent();
    assert(childExtent != null);

    final double effectiveRemainingPaintExtent = math.max(0,
        constraints.remainingPaintExtent - constraints.overlap);
    final double layoutExtent = (childExtent - constraints.scrollOffset)
        .clamp(0.0, effectiveRemainingPaintExtent) as double;

    final double cacheExtent = layoutExtent > 0.0
        ? -constraints.cacheOrigin + layoutExtent
        : layoutExtent;

    geometry = SliverGeometry(
      scrollExtent: childExtent,
      paintOrigin: constraints.overlap,
      paintExtent: math.min(childExtent, effectiveRemainingPaintExtent),
      layoutExtent: layoutExtent,
      maxPaintExtent: childExtent,
      maxScrollObstructionExtent: childExtent,
      cacheExtent: cacheExtent,
      // This rule is from native [RenderSliverPinnedPersistentHeader]:
      // Conservatively say we do have overflow to avoid complexity.
      hasVisualOverflow: true,
    );
  }

  @override
  double childMainAxisPosition(RenderBox child) => 0.0;

  double _getChildExtent() {
    switch (constraints.axis) {
      case Axis.horizontal:
        return child.size.width;
      case Axis.vertical:
        return child.size.height;
    }

    // ignore: avoid_returning_null
    return null;
  }
}