import 'package:expandable_sticky_demo/widgets/sliver_expandable_list.dart';
import 'package:expandable_sticky_demo/widgets/sliver_to_box_sticky_adapter.dart';
import 'package:flutter/material.dart';

import 'mock_data.dart';
import 'widgets/expandable_section_container.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var sectionList = MockData.getExampleSections();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxStickyAdapter(
            child: Container(
              height: 100,
              color: Colors.red,
              child: Center(
                child: Text('First sticky header'),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 100,
              color: Colors.orange,
              child: Center(
                child: Text('Regular block'),
              ),
            ),
          ),
          SliverToBoxStickyAdapter(
            child: Container(
              height: 100,
              color: Colors.yellow,
              child: Center(
                child: Text('Second sticky header'),
              ),
            ),
          ),
          SliverExpandableList(
            builder: SliverExpandableChildDelegate<String, ExampleSection>(
              sectionList: sectionList,
              itemBuilder: (context, sectionIndex, itemIndex, index) {
                String item = sectionList[sectionIndex].items[itemIndex];
                print(itemIndex);
                return ListTile(
                  leading: CircleAvatar(
                    child: Text("$index"),
                  ),
                  title: Text(item),
                );
              },
              sectionBuilder: (context, containerInfo) => _SectionWidget(
                section: sectionList[containerInfo.sectionIndex],
                containerInfo: containerInfo,
                onStateChanged: () {
                  //notify ExpandableListView that expand state has changed.
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    if (mounted) {
                      setState(() {});
                    }
                  });
                },
              ),
            ),
          ),
          SliverToBoxStickyAdapter(
            child: Container(
              height: 100,
              color: Colors.yellow,
              child: Center(
                child: Text('Third sticky header'),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 1000,
              color: Colors.grey,
              child: Center(
                child: Text('Regular block'),
              ),
            ),
          ),
        ],
      ),
    );
  }

}

class _SectionWidget extends StatefulWidget {
  final ExampleSection section;
  final ExpandableSectionContainerInfo containerInfo;
  final VoidCallback onStateChanged;

  _SectionWidget({this.section, this.containerInfo, this.onStateChanged});

  @override
  __SectionWidgetState createState() => __SectionWidgetState();
}

class __SectionWidgetState extends State<_SectionWidget>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _halfTween =
  Tween<double>(begin: 0.0, end: 0.5);
  AnimationController _controller;

  Animation _iconTurns;

  Animation<double> _heightFactor;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _iconTurns =
        _controller.drive(_halfTween.chain(CurveTween(curve: Curves.easeIn)));
    _heightFactor = _controller.drive(CurveTween(curve: Curves.easeIn));

    if (widget.section.isSectionExpanded()) {
      _controller.value = 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    widget.containerInfo
      ..header = _buildHeader()
      ..content = _buildContent();
    return ExpandableSectionContainer(
      info: widget.containerInfo,
    );
  }

  Widget _buildHeader() {
    return Container(
      color: Colors.lightBlue,
      child: ListTile(
        title: Text(
          widget.section.header,
          style: TextStyle(color: Colors.white),
        ),
        trailing: RotationTransition(
          turns: _iconTurns,
          child: const Icon(
            Icons.expand_more,
            color: Colors.white70,
          ),
        ),
        onTap: _onTap,
      ),
    );
  }

  void _onTap() {
    widget.section.setSectionExpanded(!widget.section.isSectionExpanded());
    if (widget.section.isSectionExpanded()) {
      if (mounted && widget.onStateChanged != null) {
        widget.onStateChanged();
      }
      _controller.forward().then((_) {});
    } else {
      _controller.reverse().then<void>((void value) {
        if (mounted && widget.onStateChanged != null) {
          widget.onStateChanged();
        }
      });
    }
  }

  Widget _buildContent() {
    return SizeTransition(
      sizeFactor: _heightFactor,
      child: widget.containerInfo.content,
    );
  }
}